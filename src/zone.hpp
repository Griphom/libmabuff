/*
Copyright (C) 2012-2013 Adam Matoušek

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#ifndef _MAB_ZONE_H_
#define _MAB_ZONE_H_ 0

#include "cell.hpp"
#include <vector>
#include "flags.hpp"
#include <map>
#include <istream>
#include <sstream>

namespace mabuff{

  struct BorderSetting{
    unsigned top_left;
    unsigned horizontal;
    unsigned top_right;
    unsigned vertical;
    unsigned bottom_left;
    unsigned bottom_right;
  };

  class Zone {
    protected:
      unsigned width;
      unsigned height;
      std::vector <Cell*> cells;
      int x;
      int y;
      bool visible, autohandled;
      int layer;
      std::multimap<int, Zone*>::iterator misa;
      bool is_terminator(unsigned a) const;
    public:
      Zone(const unsigned & cols, const unsigned & rows, int layer_);
      Zone(const unsigned & cols, const unsigned & rows); //If no layer is set, create an unhandled Zone
      virtual ~Zone();

      unsigned get_width() const;
      unsigned get_height() const;
      int get_x() const;
      int get_y() const;

      void fill(const unsigned & fill_ch = ' ');
      void fill(const char * pattern);
      void change_fg(const unsigned & flags_new);
      void change_bg(const unsigned & flags_new);

      void insert(std::istream & input, const unsigned & fg_flags = keep, const unsigned & bg_flags = keep); //UTF-8
      void insert(std::vector<unsigned> & input, const unsigned & fg_flags = keep, const unsigned & bg_flags = keep); //UTF-32

      template <class InputIterator> //UTF-8 or 32 (decides)
      void insert(InputIterator from, InputIterator to, const unsigned & fg_flags = keep, const unsigned & bg_flags = keep){
        bool utf8 = ((sizeof(typename InputIterator::value_type) == 1)?1:0); //true - we have some UTF-8 here
        if(utf8){
          std::stringbuf *buf = new std::stringbuf();
          std::iostream str(buf);
          while(from != to) str.put(*from++);
          insert(str, fg_flags, bg_flags);
          delete buf;
        }

        else{
          std::vector<unsigned> str;
          while(from != to) str.push_back(*from++);
          insert(str, fg_flags, bg_flags);
        }
      }

      void insert(const char* str, const unsigned & fg_flags = keep, const unsigned & bg_flags = keep); //UTF-8
      void write (std::vector<unsigned> & text, const unsigned & start_x = 0, const unsigned & start_y = 0,
                  const unsigned & text_flags = 0, const unsigned & fg_flags = keep, const unsigned & bg_flags = keep);
      void write (const char* str, const unsigned & start_x = 0, const unsigned & start_y = 0,
                  const unsigned & text_flags = 0, const unsigned & fg_flags = keep, const unsigned & bg_flags = keep);
      void set_border(unsigned fg_flags = keep, unsigned bg_flags = keep, unsigned top_left = L'┌',
                      unsigned horiz = L'─', unsigned top_right = L'┐', unsigned vert = L'│',
                      unsigned bottom_left = L'└', unsigned bottom_right = L'┘');
      void set_border(BorderSetting *setting, unsigned fg_flags = keep, unsigned bg_flags = keep);

      void print() const;

      void move(int abs_x, int abs_y);
      void move_by(int rel_x, int rel_y);// + right/down; - left/up

      void set_visibility(bool v = true);
      bool is_visible() const;
  };

}

#endif
