/*
Copyright (C) 2012-2013 Adam Matoušek

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include "mabuff.hpp"
#include "termbox.hpp"
#include "log.hpp"
#include "signal.hpp"
#include <map>
#include <system_error>

namespace mabuff{

  Zone *Root = 0; //Base zone of the whole buffer (background). Always at the bottom.
  std::multimap<int, Zone*> zones; //Layer + Zone. The higher number, the upper layer.
  void create_border_settings();
  BorderSetting * simple;
  BorderSetting * doubled;
  BorderSetting * ascii;

  void Init(unsigned width, unsigned height) throw(std::exception){
    Log(INFO, "Initialising MABuffer");

    Log(INFO, "Setting signal handlers");
    set_handlers();

    Log(INFO, "Initialisation of TermBox");
    int tbstatus = tb::init();
    if(tbstatus == tb::eunsupported_terminal){
      Log(CRIT,"You are using an unsupported terminal. We are sorry. Aborting.");
      throw std::runtime_error("Terminal not supported.");
    }
    else if(tbstatus == tb::efailed_to_open_tty){
      Log(CRIT,"Failed to open TTY. Aborting.");
      throw std::system_error(errno, std::system_category());
    }
    else if(tbstatus == tb::epipe_trap_error){
      Log(CRIT,"Pipe Trap. Aborting.");
      throw std::system_error(errno, std::system_category());
    }
    else if(tbstatus)
      Log(ERR, "Unknown error occured when initialisating TermBox. We'll see...");

    tb::select_input_mode(tb::input_esc);
    tb::clear();

    Log(INFO, "Creating Root Zone...");
    if(Root != 0) Log(WARN, "Root Zone already exists. Weird. Creating anyway");
    Root = new Zone(width, height);
    Root->fill();
    Root->change_bg(black);
    Root->change_fg(white);
    Log(INFO, "...done.");

    create_border_settings();
  }

  void Terminate(){
    Log(INFO, "Terminating MABuffer");
    delete Root;
    delete simple;
    delete doubled;
    delete ascii;
    tb::shutdown();
    Log(INFO, "Good Bye :)");
  }

  void Print(){
    Log(INFO, "Blitting to TermBox");
    Root->print();
    for(std::multimap<int, Zone*>::iterator i = zones.begin(); i != zones.end(); i++)
      (*i).second->print();
    Log(INFO, "Printing TermBox");
    tb::present();
  }

  void Wait(unsigned milliseconds){
    timespec ti;
    ti.tv_sec = milliseconds / 1000;
    ti.tv_nsec = (milliseconds % 1000) * 1000000;

    while ((nanosleep(&ti, &ti) == -1) && (errno == EINTR))
      ;
  }

  void change_default_flags(unsigned fg_flags, unsigned bg_flags){
    // BG
    if (!(fg_flags & keep))
      tb::change_default_fg(fg_flags);

    //BG
    if (!(bg_flags & keep))
      tb::change_default_bg(bg_flags);
    tb::clear();
  }

  void change_default_char(unsigned ch){
    tb::change_default_ch(ch);
    tb::clear();
  }

  void create_border_settings(){
    simple = new BorderSetting;
    doubled = new BorderSetting;
    ascii = new BorderSetting;

    simple->top_left = L'┌';
    simple->horizontal = L'─';
    simple->top_right = L'┐';
    simple->vertical = L'│';
    simple->bottom_left = L'└';
    simple->bottom_right = L'┘';

    doubled->top_left = L'╔';
    doubled->horizontal = L'═';
    doubled->top_right = L'╗';
    doubled->vertical = L'║';
    doubled->bottom_left = L'╚';
    doubled->bottom_right = L'╝';

    ascii->top_left = '+';
    ascii->horizontal = '-';
    ascii->top_right = '+';
    ascii->vertical = '|';
    ascii->bottom_left = '+';
    ascii->bottom_right = '+';
  }
}
