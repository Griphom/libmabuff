/*
Copyright (C) 2012-2013 Adam Matoušek

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include "signal.hpp"
#include <csignal>
#include <iostream>
#include "termbox.hpp"

namespace mabuff{

  void signal_handler(int sig);

  void set_handlers(){
    std::signal(SIGTERM, signal_handler);
    std::signal(SIGSEGV, signal_handler);
    std::signal(SIGINT , signal_handler);
    std::signal(SIGILL , signal_handler);
    std::signal(SIGABRT, signal_handler);
    std::signal(SIGFPE , signal_handler);
  }

  void signal_handler(int sig){
    switch(sig){
      case SIGTERM:
        std::cerr << "Received SIGTERM; MABuffer is terminating\n";
        break;
      case SIGSEGV:
        std::cerr << "Received SIGSEGV; MABuffer is terminating\n";
        break;
      case SIGINT:
        std::cerr << "Received SIGINT; MABuffer is terminating\n";
        break;
      case SIGILL:
        std::cerr << "Received SIGILL; MABuffer is terminating\n";
        break;
      case SIGABRT:
        std::cerr << "Received SIGABRT; MABuffer is terminating\n";
        break;
      case SIGFPE:
        std::cerr << "Received SIGFPE; MABuffer is terminating\n";
        break;
    }
    tb::shutdown(); // not whole mabuffer, as the program will die anyway, only
                    // to free the video, so there are no artifacts.
  }

}
